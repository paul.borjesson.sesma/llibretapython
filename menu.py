class Menu:

    def mostrar_menu_principal():
        print("\nMENÚ PRINCIPAL")
        print("============================")
        print("Seleccioni una opció i premi Intro")
        print("============================")
        print("\t1. Afegir client\n\t2. Eliminar client\n\t3. Consultar client\n\t4. Modificar un camp d'un client (*)\n\t5. Sortir")
        while (1):
            try:
                option = input("Enter an option: ")
                option = int(option)
                if option >= 1 and option <= 5:
                    return option
                else:
                    print("Valor introduit fora de rang.")
            except ValueError:
                print("Error d'entrada")

    def mostrar_menu_consulta():
        print("\nMENÚ CONSULTA")
        print("============================")
        print("Seleccioni una opció i premi Intro")
        print("============================")
        print("\t1. Cercar client per Identificador\n\t2. Cercar client per Nom\n\t3. Cercar client per Cognom\n\t4. Llistar tots els clients\n\t5. Llistar tots els clients per Nom (*)\n\t6. Enrere")
        while (1):
            try:
                option = input("Enter an option: ")
                option = int(option)
                if option >= 1 and option <= 6:
                    return option
                else:
                    print("Valor introduit fora de rang.")
            except ValueError:
                print("Error d'entrada")
