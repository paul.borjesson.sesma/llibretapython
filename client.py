import itertools

class Client:

    id = itertools.count()

    def __init__(self, nom, cognom, telefon, correu, adreça, ciutat):
        self.id = next(self.id)
        self.nom = nom
        self.cognom = cognom
        self.telefon = telefon
        self.correu = correu
        self.adreça = adreça
        self.ciutat = ciutat

    def __str__(self):
        return "ID: {}, Nom: {}, Cognom: {}, Telèfon: {}, Email: {}, Adreça: {}, Ciutat: {}".format(self.id, self.nom, self.cognom, self.telefon, self.correu, self.adreça, self.ciutat)